<!DOCTYPE html> 
<?php
	ini_set('display_errors', 1); //wlaczenie raportowania bledow
	ini_set('display_startup_errors', 1); //wlaczenie raportowania bledow
	error_reporting(E_ALL); //wlaczenie raportowania bledow
?>
<html lang="pl-PL">
    <?php
	//Dolaczenie tekstow w danym jezyku
	$lang = "pl";
	include "lang/$lang/txt.php"
	?>
	<head>
		<meta charset="UTF-8">
		<title><?php echo $txtTytulAplikacji?></title><!-- Tytul dolaczony z txt.php -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    	<link rel="stylesheet" href="CSS/style.css">	
	</head>

<body>
