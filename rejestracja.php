<?php
    session_start();
    include "inc/nagl.php";

    if(isset($_POST['email']))//nawet jak nie bedzie wartosci to zostanie zarezerwowane miejsce w pamieci
    {//Walidacja danych udana:
        $udana = true;
    
    //Sprawdzenie poprawności nicku: 
        $nick = $_POST['nick'];
    
    if(strlen($nick)<2 || strlen($nick)>15)
        {
            $udana=false;
            $_SESSION['nick_error']='Podany nick musi posiadać od 2 do 15 znaków.';
        }

        if(ctype_alnum($nick)==false)
        {$udana = false;
            $_SESSION['nick_error']='Nick może składać się jedynie z liter i cyfr.';
        }

    //Sprawdzenie poprawności adresu email:   
    $email = $_POST['email'];
    $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

    if((filter_var($emailB, FILTER_VALIDATE_EMAIL))==false || ($emailB!=$email))
        {$udana = false;
        $_SESSION['email_error']='Podaj poprawny adres email.';
        }
    
    //Sprawdzenie poprawności i zgodności haseł:
    
    $haslo1 = $_POST['haslo_rejestracji'];
    $haslo2 = $_POST['haslo_powtorz'];
  
    if((strlen($haslo1)<8)||(strlen($haslo2)>15))
    {   $udana = false;
        $_SESSION['pass_error']='Hasło musi posiadać od 8 do 20 znaków.';
    }

    $haslo_hash=password_hash($haslo1, PASSWORD_DEFAULT);
    //echo $haslo_hash; exit();
    

    if($haslo1 != $haslo2)
    {   $udana=false;
        $_SESSION['pass_error']='Podane hasła nie są identyczne.';
    }
    // Sprawdzanie zaznaczenia checkboxa;
    if(!isset($_POST['regulamin']))
    {   $udana = false;
        $_SESSION['reg_error']='Zaakceptuj regulamin.';
    }

    require_once "inc/baza.php";
    mysqli_report(MYSQLI_REPORT_STRICT);
    try
    {
        $polaczenie = new mysqli($serwerDB, $login_baza, $haslo_baza, $baza);
        if($polaczenie->connect_errno!=0)
        {
            throw new Exception(mysqli_connect_errno());
        }
        else
        {   //Sprawdzamy czy email jest juz w bazie:
            $rezultat=$polaczenie->query("SELECT id FROM uzytkownicy WHERE email='$email'");
            
            if(!$rezultat) 
            throw new Exception($polaczenie->error);

            $ile_identycznych_emaili = $rezultat->num_rows;
            if($ile_identycznych_emaili>0)
            {
                $udana = false;
                $_SESSION['email_error']='Istnieje już konto powiązane z tym adresem email.';
            }

            //Sprawdzamy czy nick jest juz w bazie:
            $rezultat=$polaczenie->query("SELECT id FROM uzytkownicy WHERE login='$nick'");
                
            if(!$rezultat) throw new Exception($polaczenie->error);

            $ile_identycznych_nickow = $rezultat->num_rows;
                if($ile_identycznych_nickow>0)
                    {
                        $udana = false;
                        $_SESSION['nick_error']='Istnieje już użytkownik o takiej nazwie.';
                    }
            

               if ($udana==true)
            {
                        if($polaczenie->query("INSERT INTO uzytkownicy VALUES (NULL, '$nick', '$haslo_hash', '$email')"))
                        {
                            $_SESSION['udanarejestracja'] = true;
                            header('Location: index.php');
                        }
                        else
                            {
                                throw new Exception($polaczenie->error);
                            }
            }
        
            $polaczenie->close();
        }
    }

    catch(Exception $e)
    {
        echo "Błąd serwera!";
        echo '<br />'.$e; 
    }
    }

?>

<title> Zarejestruj się</title>
<form  method="post"> <!-- ten sam plik przetworzy dane gdy nie ma action-->
    Nazwa użytkownika: <br> <input type ="txt" name="nick"/> <br>
    
    <?php

    if(isset($_SESSION['nick_error']))
    {echo '<div style="color:red">'.$_SESSION['nick_error'].' </div>';
    unset($_SESSION['nick_error']);}

    ?>

    Hasło: <br> <input type ="password" name="haslo_rejestracji"/> <br>
    
    <?php
    if(isset($_SESSION['pass_error']))
        {echo '<div style="color:red">'.$_SESSION['pass_error'].' </div>';
        unset($_SESSION['pass_error']);}
    ?>
    Powtórz hasło: <br> <input type ="password" name="haslo_powtorz"/> <br>
    Email: <br> <input type ="txt" name="email"/> <br>
    <?php

        if(isset($_SESSION['email_error']))
        {echo '<div style="color:red">'.$_SESSION['email_error'].' </div>';
        unset($_SESSION['email_error']);}
    ?>

    <label>
        <input type="checkbox" name="regulamin"/> Akceptuję regulamin
    </label>
    <?php
        if(isset($_SESSION['reg_error']))
        {echo '<div style="color:red">'.$_SESSION['reg_error'].' </div>';
        unset($_SESSION['reg_error']);}
    ?>


    <br>
    <input type="submit" value="Zarejestruj się" />
</form>
[<a href="index.php"> Zaloguj się </a>]</p>


<?php 
    if(isset($_SESSION['blad']))
    {echo $_SESSION['blad'];}

    include "inc/stopka.php"; 
?>