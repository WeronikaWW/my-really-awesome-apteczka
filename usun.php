<?php
    session_start();

    if(!isset($_SESSION['zalogowany']))
    {
        header('Location: index.php');
        exit();
    }
    
    $zalogowany = $_SESSION['zalogowany'];
    include 'inc/nagl.php';
    echo "<p>Witaj ".$_SESSION['login'].'! [<a href="wyloguj.php"> Wyloguj się </a>]</p>';

    require_once "inc/baza.php";
    mysqli_report(MYSQLI_REPORT_STRICT);

    $id_apteczki= $_SESSION['id_apteczki'];

    try{
        $polaczenie = new mysqli($serwerDB, $login_baza, $haslo_baza, $baza);
        if($polaczenie->connect_errno!=0){
            throw new Exception(mysqli_connect_errno());
        }
        else{
            $rezultat=$polaczenie->query("SELECT leki.id_leku, leki.nazwa_leku, leki.opakowanie, leki_w_apteczkach.data_waznosci, leki_w_apteczkach.id_leku_w_apteczce FROM leki,leki_w_apteczkach WHERE leki.id_leku=leki_w_apteczkach.id_leku AND Id_apteczki=$id_apteczki");
            $data = date('Y-m-d H:i:s');
            if($_POST['action']=="Utylizuj")
                {
                    while($wynik = $rezultat->fetch_assoc()){
                    if(isset($_POST['utylizacja'.$wynik['id_leku_w_apteczce']]))
                        {
                            $operacja = $_POST['utylizacja'.$wynik['id_leku_w_apteczce']];
                            $id_leku = $wynik['id_leku'];
                            $ilosc = 1;
                            $koszty = 0;

                            $polaczenie->query("INSERT INTO operacje VALUES (NULL, '$operacja', '$zalogowany', '$id_leku', '$id_apteczki', '$ilosc', '$koszty', '$data' )" );
                            $polaczenie->query('DELETE FROM leki_w_apteczkach WHERE id_leku_w_apteczce='.$wynik['id_leku_w_apteczce']);
                        }
                    }
                }
            else if($_POST['action']=="Zażyj")
                {
                    while($wynik = $rezultat->fetch_assoc())
                    {
                        if(isset($_POST['zazycie'.$wynik['id_leku_w_apteczce']]))
                            {
                                $operacja = $_POST['zazycie'.$wynik['id_leku_w_apteczce']];
                                $id_leku = $wynik['id_leku'];
                                $ilosc = 1;
                                $koszty = 0;

                                $polaczenie->query("INSERT INTO operacje VALUES (NULL, '$operacja', '$zalogowany', '$id_leku', '$id_apteczki', '$ilosc', '$koszty', '$data' )" );
                                $polaczenie->query('DELETE FROM leki_w_apteczkach WHERE id_leku_w_apteczce='.$wynik['id_leku_w_apteczce']);
                            }
                    }

                }            
            
            $_SESSION['po_usuwaniu'] = $id_apteczki;
            $polaczenie->close();
        }
    }
    catch(Exception $e){
        echo "Błąd serwera! Przepraszamy za niedogodności i prosimy o rejsetracje w innym terminie";
        echo '<br>Bład'.$e;
    }
?>

<div><p style="text-align:center"> Lek został usunięty z apteczki! </p></div>

<a class="btn btn-primary btn-block" href = 'stan.php'>Zobacz stan apteczki</a>
<a class="btn btn-primary btn-block" href = 'menu.php'>Powrót do menu</a>

<?php
    include 'inc/stopka.php';
?>