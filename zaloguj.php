<?php
    session_start();

    if((!isset($_POST['login'])) || (!isset($_POST['haslo'])))
    {header('Location: index.php');
    exit();}
     
    require_once "inc/baza.php";
    $polaczenie = polaczenie($serwerDB, $login_baza, $haslo_baza, $baza);

    if($polaczenie->connect_errno!=0)
    {
        echo "Error: ".$polaczenie->connect_erno. "Opis: ".$polaczenie->connect_errno;
    }
    else
    {
        $login = $_POST['login'];
        $haslo = $_POST['haslo']; 

        $login = htmlentities($login, ENT_QUOTES, "UTF-8");
	
		if ($rezultat = @$polaczenie->query(
		sprintf("SELECT * FROM uzytkownicy WHERE login='%s'",
		mysqli_real_escape_string($polaczenie,$login))))
		{
            $ilu_uzytkownikow = $rezultat->num_rows;
            if($ilu_uzytkownikow>0)
            {   
                $wiersz = $rezultat->fetch_assoc(); 

                if (password_verify($haslo, $wiersz['haslo']))
                {   
                    $_SESSION['zalogowany'] = $wiersz['id'];

                    $_SESSION['id'] = $wiersz['id'];
                    $_SESSION['login'] = $wiersz['login'];
                    $_SESSION['haslo'] = $wiersz['haslo'];
                    $_SESSION['email'] = $wiersz['email'];

                    unset($_SESSION['blad']);
                    $rezultat->free_result();
                    header('Location: menu.php');
                }   
                else{
                    $_SESSION['blad'] = '<span style="color:red"> <b>Nieprawidłowy login lub hasło! </b></span>';
                    header('Location: index.php');

                }
            }else {
                $_SESSION['blad'] = '<span style="color:red"> <b>Nieprawidłowy login lub hasło! </b></span>';
                header('Location: index.php');
            }
        }

        $polaczenie->close();
    }

?>
