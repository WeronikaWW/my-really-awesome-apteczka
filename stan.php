<?php
    session_start();

    if(!isset($_SESSION['zalogowany']))
    {
        header('Location: index.php');
        exit();
    }
    
    $zalogowany = $_SESSION['zalogowany'];
    include 'inc/nagl.php';
    echo "<p>Witaj ".$_SESSION['login'].'! [<a href="wyloguj.php"> Wyloguj się </a>]</p>';
?>

<div class = "container">
        <div class="row">
            <form method = 'post'>
                <div class ="form-group row">
                    <div class="form-group col-md-6">
                        <label for="apteczka">Wybierz apteczkę</label>
                        <select id="apteczka" name = "apteczka">

                            <?php
                                include 'wyswietlapteczki.php';
                            ?>

                        </select> 
                    </div>
                    <div class="form-group col-md-6">
                        <input class = "btn btn-primary btn-block" type="submit" value = "Sprawdź">  
                    </div>
                </div> 
            </form>
        </div>

        <?php
            if(isset($_POST['apteczka']) || isset($_SESSION['po_usuwaniu'])){
                if(isset($_POST['apteczka'])){
                    $id_apteczki = $_POST['apteczka'];
                }
                else{
                    $id_apteczki = $_SESSION['po_usuwaniu'];
                }
                $_SESSION['id_apteczki'] = $id_apteczki;
                
                    require_once 'inc/baza.php';
                    mysqli_report(MYSQLI_REPORT_STRICT);

                    try{  
                        $polaczenie = new mysqli($serwerDB, $login_baza, $haslo_baza, $baza);
                        if($polaczenie->connect_errno!=0){
                            throw new Exception(mysqli_connect_errno());
                        }
                        else{
                            $rezultaty = $polaczenie->query("SELECT leki.nazwa_leku, leki.opakowanie, leki_w_apteczkach.data_waznosci, leki_w_apteczkach.id_leku_w_apteczce FROM leki,leki_w_apteczkach WHERE leki.id_leku=leki_w_apteczkach.id_leku AND Id_apteczki=$id_apteczki");
                            if(!$rezultaty) throw new Exception($polaczenie->error);
                        
                            else{
                                echo '<div id = "1" class="row">';
                                echo '<form action = "usun.php" method = "post">';
                                echo '<div class = "form-group row">';
                                echo '<table class="table"><th>Nazwa leku</th><th>Opakowanie</th><th>Data ważności</th><th>Status Leku</th><th><div><input type="submit" name = "action" value="Utylizuj"></div></th><th><div><input type="submit" name = "action" value="Zażyj"></div></th>';
                                while($wynik = $rezultaty->fetch_assoc()){
                                    $data = new DateTime($wynik['data_waznosci']);    
                                    if($data->format('Y-m-d hh:ii:ss')<date('Y-m-d hh:ii:ss'))
                                        echo '<tr><td>'.$wynik['nazwa_leku'].'</td><td>'.$wynik['opakowanie'].'</td><td>'.$wynik['data_waznosci'].'</td><td  style="color:red"> Lek przeterminowany</td> <td><input type="checkbox" name="utylizacja'.$wynik['id_leku_w_apteczce'].'" value = "utylizacja"></td></tr>';
                                    else{
                                        echo '<tr><td>'.$wynik['nazwa_leku'].'</td><td>'.$wynik['opakowanie'].'</td><td>'.$wynik['data_waznosci'].'</td><td  style="color:green"> Lek zdatny do spożycia</td><td><input type="checkbox"  name=zazycie'.$wynik['id_leku_w_apteczce'].' value = "zazycie"></td></tr>';// <td><input type="checkbox"  name='.$wynik['id_leku_w_apteczce'].' value = "zazycie"></td></tr>';
                                    }
                                }
                                echo '</table>';
                                echo '</div>';
                                echo '</form>';
                                echo '</div>';
                            }
                            $rezultaty->free_result();
                            $polaczenie->close();
                            
                        }
                    }
                    catch(Exception $e){
                        echo $e->getMessage();
                        echo "blad polaczenia z baza";
                    }

                    unset($_POST['apteczka']);
                    unset($_SESSION['po_usuwaniu']);
            }
        ?>
        <div class="row">
                <div class = "col-md-4">
                    <a class="btn btn-primary btn-block" href = 'menu.php'>Wróć do menu</a>
                </div>
        </div>
   
</div>
        

<?php
    include 'inc/stopka.php';
?>