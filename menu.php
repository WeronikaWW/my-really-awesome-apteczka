<?php
    session_start();

    if(!isset($_SESSION['zalogowany']))
    {
        header('Location: index.php');
        exit();
    }
    
    $zalogowany = $_SESSION['zalogowany'];
    include 'inc/nagl.php';
    echo "<p>Witaj ".$_SESSION['login'].'! [<a href="wyloguj.php"> Wyloguj się </a>]</p>';
?>

<ul class="list-group" style="max-width:400px;overflow: auto;">
    <li class="list-group-item"><a href = 'dodajapteczke.php'>Dodaj apteczkę</a></li>
    <li class="list-group-item"><a href = 'dodajlek.php'>Dodaj lek</a></p></li>
    <li class="list-group-item"><a href = 'stan.php'>Stan apteczki</a></li>
    <li class="list-group-item"><a href = 'historia.php'>Historia leków</a></li>
</ul>
<?php
    include 'inc/stopka.php';
?>


