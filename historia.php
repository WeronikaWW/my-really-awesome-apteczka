<?php
    session_start();

    if(!isset($_SESSION['zalogowany']))
    {
        header('Location: index.php');
        exit();
    }
    
    $zalogowany = $_SESSION['zalogowany'];
    include 'inc/nagl.php';
    echo "<p>Witaj ".$_SESSION['login'].'! [<a href="wyloguj.php"> Wyloguj się </a>]</p>';
?>

<div class = "containter">
<div class = "row">
    <form method = 'post'>
        <div class = "form-group row">
            <label for="apteczka">_____Historia apteczki:</label>
            <select id="apteczka" name = "apteczka">

                <?php
                    include 'wyswietlapteczki.php';
                ?>

            </select> 

            <input type="submit" value = "Wyświetl">    
        </div>
    </form>
</div>
    <?php

        if(isset($_POST['apteczka'])){
            $id_apteczki = $_POST['apteczka'];
            echo '<input type="text" id="myInput1" onkeyup="myFunction(0,id)" placeholder="Szukaj operacji.." title="Type in a name">';
            echo '<input type="text" id="myInput2" onkeyup="myFunction(1,id)" placeholder="Szukaj leku.." title="Type in a name">';
            echo '<input type="text" id="myInput3" onkeyup="myFunction(2,id)" placeholder="Szukaj uzytkownika.." title="Type in a name">';
            echo '<label for="data1">Od: </label>';
            echo '<input type="date" id = "data1" name="datawaznosci">';
            echo '<label for="data2">Do: </label>';
            echo '<input type="date" id = "data2" name="datawaznosci">';
            echo '<button type="button" id = "button1" onclick = "showDate()" >Filtruj datę</button>';
            echo '<table class = "table" id = "myTable"><th>Rodzaj operacji</th><th>Nazwa leku</th><th>Uzytkownik</th><th>Data</th>';
            require_once 'inc/baza.php';
            mysqli_report(MYSQLI_REPORT_STRICT);

            try{  
                $polaczenie = new mysqli($serwerDB, $login_baza, $haslo_baza, $baza);
                if($polaczenie->connect_errno!=0){
                    throw new Exception(mysqli_connect_errno());
                }
                else{
                    $rezultaty = $polaczenie->query("SELECT operacje.rodzaj, leki.nazwa_leku, uzytkownicy.login, operacje.data FROM operacje,leki,uzytkownicy WHERE operacje.id_leku=leki.id_leku AND operacje.id_uzytkownika=uzytkownicy.id AND operacje.id_apteczki=$id_apteczki ORDER BY operacje.data DESC");
                    if(!$rezultaty) throw new Exception($polaczenie->error);
                    else{
                        while($wynik = $rezultaty->fetch_assoc()){
                                echo '<tr><td>'.$wynik['rodzaj'].'</td><td>'.$wynik['nazwa_leku'].'</td><td>'.$wynik['login'].'</td><td>'.$wynik['data'].'</td></tr>';
                            
                        }
                    }
                    $rezultaty->free_result();
                    $polaczenie->close();
                    
                }
            }
            catch(Exception $e){
                echo $e->getMessage();
                echo "blad polaczenia z baza";
            }
        }


    ?>


    <div class = "col-md-4">
                    <a class="btn btn-primary btn-block" href = 'menu.php'>Wróć do menu</a>
    </div>
</div>

<script src = "js/skrypt.js"> </script>

<?php
    include 'inc/stopka.php';
?>